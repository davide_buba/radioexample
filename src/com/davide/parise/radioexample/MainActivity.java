package com.davide.parise.radioexample;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;

import com.davide.parise.radioexample.onair.OnairService;

public class MainActivity extends ActionBarActivity{

	MainPageAdapter pageAdapter;
	ViewPager viewPager;
	OnairService onairService;
	
	/*
	 * Bind service callback
	 */
	private ServiceConnection mConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			// TODO Auto-generated method stub
			onairService = ((OnairService.LocalBinder) service).getService();
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			// TODO Auto-generated method stub
			onairService = null;
		}

	};

	/* ActionBar Tab listener */
	ActionBar.TabListener tabListener = new ActionBar.TabListener() {

		@Override
		public void onTabReselected(Tab arg0, FragmentTransaction arg1) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onTabSelected(Tab arg0, FragmentTransaction arg1) {
			// TODO Auto-generated method stub
			viewPager.setCurrentItem(arg0.getPosition());
		}

		@Override
		public void onTabUnselected(Tab arg0, FragmentTransaction arg1) {
			// TODO Auto-generated method stub
			
		}
		
	};
	
	/* page change listener */
	ViewPager.SimpleOnPageChangeListener pagerListener = new ViewPager.SimpleOnPageChangeListener(){

		/* (non-Javadoc)
		 * @see android.support.v4.view.ViewPager.SimpleOnPageChangeListener#onPageSelected(int)
		 */
		@Override
		public void onPageSelected(int position) {
			// TODO Auto-generated method stub
            getSupportActionBar().setSelectedNavigationItem(position);

		}
		
	};
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		pageAdapter = new MainPageAdapter(getSupportFragmentManager());
		viewPager = (ViewPager) findViewById(R.id.pager);
		viewPager.setAdapter(pageAdapter);
		viewPager.setOnPageChangeListener(pagerListener);

		final ActionBar actionBar = getSupportActionBar();
	    actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
	    
	    // create new tabs
	    Tab tabOnair = actionBar.newTab();
	    tabOnair.setText("OnAir");
	    tabOnair.setTabListener(tabListener);
	    Tab tabPodcast = actionBar.newTab();
	    tabPodcast.setText("Podcast");
	    tabPodcast.setTabListener(tabListener);
	    Tab tabSocial = actionBar.newTab();
	    tabSocial.setText("Social");
	    tabSocial.setTabListener(tabListener);
	    
	    // add tab to actionBar
	    actionBar.addTab(tabOnair);	    
	    actionBar.addTab(tabPodcast);
	    actionBar.addTab(tabSocial);
	    
		Intent intent = new Intent(this, OnairService.class);		
		bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	
	/* (non-Javadoc)
	 * @see android.support.v7.app.ActionBarActivity#onStop()
	 */
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		Log.i(MainActivity.class.getSimpleName(), "onStop");
		if(onairService!=null && onairService.isPlaying())
			onairService.startForeground();
	}

	/* (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onStart()
	 */
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		Log.i(MainActivity.class.getSimpleName(), "onStart");
		if(onairService!=null && onairService.isPlaying())
			onairService.stopForeground();
		else
			Log.e(MainActivity.class.getSimpleName(), "onStart - service is null");
	}

}
