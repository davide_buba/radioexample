package com.davide.parise.radioexample.onair;

import org.eclipse.jdt.annotation.NonNull;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.davide.parise.radioexample.R;

public class OnairFragment extends Fragment {

	private OnairService onairService;
	private Button buttonPlay;
	private ProgressBar progress;
	private TextView textViewError;
	
	private String player_error;
	
	/*
	 * Bind service callback
	 */
	private ServiceConnection mConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			// TODO Auto-generated method stub
			onairService = ((OnairService.LocalBinder) service).getService();
			onairService.setExternListener(listenerOnairService);
			if(!onairService.isServiceStarted()){
				Log.i(OnairFragment.class.getName(), "Starting Service");
				Intent intent = new Intent(getActivity(), OnairService.class);
				getActivity().startService(intent);
			}
			updateUI();
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			// TODO Auto-generated method stub
			onairService = null;
		}

	};

	private OnairService.OnairServiceListener listenerOnairService = new OnairService.OnairServiceListener() {

		@Override
		public void onPrepareException(@NonNull Exception e) {
			// TODO Auto-generated method stub
			Toast.makeText(getActivity(), "OnPrepareExceptio " + e.getMessage(), Toast.LENGTH_LONG)
					.show();
			player_error = e.getMessage();
			updateUI();
		}

		@Override
		public void onPostPrepare() {
			// TODO Auto-generated method stub
			Toast.makeText(getActivity(), "READY TO PLAY", Toast.LENGTH_LONG).show();
			player_error = null;
			updateUI();
		}

		@Override
		public void onPlayerError(int what, int extra) {
			// TODO Auto-generated method stub
			Toast.makeText(getActivity(), "Player Error:" + what, Toast.LENGTH_LONG).show();
			player_error = "Player Error Code:" + what;
			updateUI();
		}

		@Override
		public void onAudioError() {
			// TODO Auto-generated method stub
			Toast.makeText(getActivity(), "Audio Error:", Toast.LENGTH_LONG).show();
			player_error = "Audio Error:";
			updateUI();
		}
	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater,
	 * android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		ViewGroup rootView = (ViewGroup) inflater.inflate(
				R.layout.onair_fragment, container, false);
		return rootView;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onActivityCreated(android.os.Bundle)
	 */
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		Log.i("OnAirFragment", "onActivityCreated");
		
		Intent intent = new Intent(getActivity(), OnairService.class);		
		getActivity().bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
		

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onViewCreated(android.view.View,
	 * android.os.Bundle)
	 */
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		progress = (ProgressBar) view.findViewById(R.id.progressBar_onair);	
		textViewError = (TextView) view.findViewById(R.id.textView_onair_error);
		buttonPlay = (Button) view.findViewById(R.id.button_onair_play);
		buttonPlay.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub  
				if (onairService != null) { 
					onairService.toogleMusic();
				}
			}

		});
		
		updateUI();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onDestroy()
	 */
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		Log.i("OnAirFragment", "onDestroy");
		getActivity().unbindService(mConnection);
	}

	

	/**
	 * Find the best view to display
	 * Change the visibility of Views and text to show
	 */
	private void updateUI() {
		// TODO Auto-generated method stub
		if(onairService==null)
			viewForWait();
		else if(player_error==null)
			viewForPlay();
		else
			viewForError();
	}

	/**
	 * Change the visibility of components views
	 * Call it for show Play button
	 */
	private void viewForPlay(){
		Log.i(this.getClass().getSimpleName(), "viewForPlay");
		progress.setVisibility(View.GONE);
		textViewError.setVisibility(View.GONE);
		buttonPlay.setVisibility(View.VISIBLE);
		if(onairService!=null && onairService.isPlayerReady())
			buttonPlay.setEnabled(true);
		else{
			buttonPlay.setEnabled(false);
		}
	}
	/**
	 * Change the visibility of components for wait service
	 * Call it for show progressBar 
	 */
	private void viewForWait(){
		Log.i(this.getClass().getName(), "viewForWait");
		progress.setVisibility(View.VISIBLE);
		buttonPlay.setVisibility(View.GONE);	
		textViewError.setVisibility(View.GONE);
	}
	
	/**
	 * Change visibility of the components for Error state
	 * Show a text error
	 */
	private void viewForError(){
		Log.i(this.getClass().getName(), "viewForError");
		progress.setVisibility(View.GONE);
		buttonPlay.setVisibility(View.GONE);
		textViewError.setVisibility(View.VISIBLE);
		if(player_error==null)
			player_error = "Unknow error";
		textViewError.setText(player_error);
	}
}
