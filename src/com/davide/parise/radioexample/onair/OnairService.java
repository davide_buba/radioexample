package com.davide.parise.radioexample.onair;

import java.io.IOException;

import org.eclipse.jdt.annotation.NonNull;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.WifiLock;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.davide.parise.radioexample.MainActivity;

/**
 * 
 * @author davide Service for Play streaming music
 * 
 */
public class OnairService extends Service implements MediaPlayer.OnPreparedListener,
		MediaPlayer.OnErrorListener, AudioManager.OnAudioFocusChangeListener {

	public static final String URL = "http://fr3.ah.fm:9000/";
	private static final int NOTIFICATION_ID = 1;

	private final IBinder mBinder = new LocalBinder();
	private MediaPlayer player;

	private OnairServiceListener externListener;
	private boolean isServiceStarted;
	private boolean isPlayerReady;

	private WifiLock wifiLock;

	/**
	 * Public interface for handle function to caller
	 */
	public interface OnairServiceListener {
		public void onPrepareException(@NonNull Exception e);

		public void onPostPrepare();

		public void onPlayerError(int what, int extra);

		public void onAudioError();
	}

	/**
	 * Class used for the client Binder. Because we know this service always
	 * runs in the same process as its clients, we don't need to deal with IPC.
	 */
	public class LocalBinder extends Binder {
		public OnairService getService() {
			// Return this instance of LocalService so clients can call public
			// methods
			return OnairService.this;
		}
	}

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return mBinder;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Service#onStartCommand(android.content.Intent, int, int)
	 */
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		Log.i("service", "onstart");
		if (initMediaPlayer()) {
			return START_NOT_STICKY;
		}
		return START_STICKY;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Service#onCreate()
	 */
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		wifiLock = ((WifiManager) getSystemService(Context.WIFI_SERVICE)).createWifiLock(
				WifiManager.WIFI_MODE_FULL, "mylock");

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Service#onDestroy()
	 */
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		wifiLock.release(); // wifi is unlocked
		Log.d("service", "onDestroy");
		this.isServiceStarted = false;

		if (player != null)
			player.release();
		player = null;
	}

	/**
	 * Initialize MediaPlayer and prepare it with asyncTask
	 * 
	 * @return - True if media player is initialize, false if an error occur
	 */
	private boolean initMediaPlayer() {
		// TODO Auto-generated method stub
		if (player != null)
			player.release();
		player = new MediaPlayer();
		player.setAudioStreamType(AudioManager.STREAM_MUSIC);
		player.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK); // wake
																						// lock
		try {
			Log.i("service", "onstart - PREPARE");
			player.setDataSource(URL);
			player.setOnPreparedListener(this);
			player.setOnErrorListener(this);
			player.prepareAsync();
			Log.i("service", "onstart - PREPARED");
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if (this.externListener != null)
				this.externListener.onPrepareException(e);
			return false;
		} catch (SecurityException e) {
			e.printStackTrace();
			// TODO Auto-generated catch block
			if (this.externListener != null)
				this.externListener.onPrepareException(e);
			return false;
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if (this.externListener != null)
				this.externListener.onPrepareException(e);
			return false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if (this.externListener != null)
				this.externListener.onPrepareException(e);
			return false;
		}
		this.isServiceStarted = true;
		wifiLock.acquire();
		return true;
	}

	/****************************************************************************/
	/************************ NOTIFICATIONS *************************************/
	/****************************************************************************/

	/**
	 * Get the notification when start into foreground Use the same ID for
	 * update and/or remove
	 * 
	 * @param title
	 *            - title to show
	 * @param text
	 *            - Other detail text to show
	 * 
	 * @return - The notification created
	 */
	private Notification getNotification(String title, String text) {
		PendingIntent pi = PendingIntent.getActivity(getApplicationContext(), 0, new Intent(
				getApplicationContext(), MainActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);

		NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext());
		builder.setAutoCancel(true);
		builder.setContentTitle(title);
		builder.setContentText(text);
		builder.setSmallIcon(android.R.drawable.ic_dialog_info);
		builder.setContentIntent(pi);
		return builder.build();

	}

	/**
	 * Start the service into foreground mode. Must show the notification to the
	 * user
	 */
	public void startForeground() {
		Notification notification = getNotification("RadioExample", "Streaming Music");
		NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

		notificationManager.notify(NOTIFICATION_ID, notification);
	}

	/**
	 * Start the service into background mode; Remove all notification displayed
	 */
	public void stopForeground() {
		NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

		notificationManager.cancel(NOTIFICATION_ID);
	}

	/***********************************************************/
	/******************* MEDIA PLAYER HANDLER ******************/
	/***********************************************************/
	@Override
	public boolean onError(MediaPlayer mp, int what, int extra) {
		// TODO Auto-generated method stub
		if (this.externListener != null)
			this.externListener.onPlayerError(what, extra);

		// handle error here ...
		isPlayerReady = false;
		return true;
	}

	@Override
	public void onPrepared(MediaPlayer mp) {
		// TODO Auto-generated method stub
		isPlayerReady = true;
		
		if (this.externListener != null)
			this.externListener.onPostPrepare();
	}

	/*****************************************************************/
	/*************** AUDIO FOCUS CHANGE LISTENER *********************/
	/*****************************************************************/

	/**
	 * Request to grant the audio focus
	 * 
	 * @return - true if the App grant the focus, false otherwise
	 */
	public boolean grantAudioFocus() {
		AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		int result = audioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC,
				AudioManager.AUDIOFOCUS_GAIN);

		if (result != AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
			// could not get audio focus.
			return false;
		}
		return true;
	}

	@Override
	public void onAudioFocusChange(int focusChange) {
		// TODO Auto-generated method stub
		switch (focusChange) {
		case AudioManager.AUDIOFOCUS_GAIN:
			// resume playback
			if (player == null)
				initMediaPlayer();
			else if (!player.isPlaying())
				player.start();
			player.setVolume(1.0f, 1.0f);
			break;

		case AudioManager.AUDIOFOCUS_LOSS:
			// Lost focus for an unbounded amount of time: stop playback and
			// release media player
			if (player != null) {
				if (player.isPlaying())
					player.stop();
				player.release();
				player = null;
				isPlayerReady = false;
			}
			break;

		case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
			// Lost focus for a short time, but we have to stop
			// playback. We don't release the media player because playback
			// is likely to resume
			if (player != null && player.isPlaying())
				player.pause();
			break;

		case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
			// Lost focus for a short time, but it's ok to keep playing
			// at an attenuated level
			if (player != null && player.isPlaying())
				player.setVolume(0.1f, 0.1f);
			break;
		}
	}

	/***********************************************************/
	/***************** PUBLIC UTIL FUNCTIONS *******************/
	/***********************************************************/

	/**
	 * Check if the service is started
	 * 
	 * @return the isStarted
	 */
	public boolean isServiceStarted() {
		return isServiceStarted;
	}

	/**
	 * Reinitialize media player Can be useful for handle errors
	 */
	public void restartPlayer() {
		if (player != null)
			player.reset();
		else
			player = new MediaPlayer();
		initMediaPlayer();
	}

	/**
	 * If it is in play call pause otherwise call play
	 */
	public void toogleMusic() {
		Log.i("service", "tooleMusic");
		if (player == null)
			return; // safety check

		if (player.isPlaying())
			player.pause();
		else {
			if (grantAudioFocus()) {
				player.start();
			} else {
				if (this.externListener != null)
					this.externListener.onAudioError();
			}
		}

	}

	/**
	 * Pause the streaming
	 */
	public void pause() {
		// TODO Auto-generated method stub
		Log.i("service", "pause");
		if (player != null && player.isPlaying()) {
			player.pause();
		} else {
			Log.e("service", "PLAYER IS NULL");
		}
	}

	/**
	 * Play the streaming
	 */
	public void play() {
		// TODO Auto-generated method stub
		if (player != null && !player.isPlaying()) {
			if (grantAudioFocus()) {
				player.start();
			} else {
				if (this.externListener != null)
					this.externListener.onAudioError();
			}
		}
	}

	/**
	 * 
	 * @return - true if is in playing
	 */
	public boolean isPlaying() {
		if (player != null && player.isPlaying())
			return true;
		return false;
	}

	/***********************************************************/
	/************************* GET & SET ***********************/
	/***********************************************************/

	/**
	 * @return the externListener
	 */
	public OnairServiceListener getExternListener() {
		return externListener;
	}

	/**
	 * @param externListener
	 *            the externListener to set
	 */
	public void setExternListener(OnairServiceListener externListener) {
		this.externListener = externListener;
	}

	/**
	 * @return true if the player is prepared and ready to play the stream od
	 *         music false otherwise.
	 */
	public boolean isPlayerReady() {
		return isPlayerReady;
	}

}
