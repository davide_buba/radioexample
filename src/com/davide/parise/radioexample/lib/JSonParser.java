package com.davide.parise.radioexample.lib;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.stream.JsonReader;

public class JSonParser {

	/**
	 * Read element "nom" from the JSon InputStream
	 *  
	 * @param in - the input stream where read the JSon file
	 * @return - the List of element that have read
	 * @throws UnsupportedEncodingException - throw an exception if an error occur
	 * 
	 */
	public List<String> readElement(InputStream in) throws UnsupportedEncodingException {
		List<String> messages = new ArrayList<String>();

		JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
		try {
			reader.beginObject();
			while (reader.hasNext()) {
				String name = reader.nextName();

				if (name.equals("programes") || name.equals("podcasts")) {

					reader.beginArray();
					while (reader.hasNext()) {
						// read programs
						reader.beginObject();
						while (reader.hasNext()) {
							String nom = reader.nextName();
							if (nom.equals("nom")) {
								String value = reader.nextString();
								messages.add(value);
							} else {
								reader.skipValue();
							}
						}
						reader.endObject();
					}
					reader.endArray();
				}

				else {
					reader.skipValue();
				}
			}
			reader.endObject();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return messages;
	}
}
