package com.davide.parise.radioexample.social;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.widget.Toast;

import com.davide.parise.radioexample.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.plus.PlusClient;

public class GoogleSocialActivity extends ActionBarActivity {
	//private GoogleApiClient mGoogleApiClient;

	private PlusClient mPlusClient;

	private ConnectionCallbacks listener_connection = new ConnectionCallbacks(){

		@Override
		public void onConnected(Bundle arg0) {
			// TODO Auto-generated method stub
			Log.i("GoogleSocial", "onConnected");
			Toast.makeText(GoogleSocialActivity.this, "Connected", Toast.LENGTH_LONG).show();
		}

		@Override
		public void onDisconnected() {
			// TODO Auto-generated method stub
			Log.i("GoogleSocial", "onDisconnected");
			Toast.makeText(GoogleSocialActivity.this, "Disconnected", Toast.LENGTH_LONG).show();
			
		}
		
	};
	private OnConnectionFailedListener lister_connection_failed = new OnConnectionFailedListener(){

		@Override
		public void onConnectionFailed(ConnectionResult arg0) {
			// TODO Auto-generated method stub
			Log.i("GoogleSocial", "onConnection Fails "+arg0.toString());
			Toast.makeText(GoogleSocialActivity.this, "Connection Failes :"+arg0.getErrorCode(), Toast.LENGTH_LONG).show();
		}
		
	};
	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v7.app.ActionBarActivity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.social_activity);
		
		 mPlusClient = new PlusClient.Builder(this, listener_connection, lister_connection_failed)
	        .setActions("http://schemas.google.com/AddActivity", "http://schemas.google.com/BuyActivity")
	        .build();
		// Google play lib rev 15
		/*
		mGoogleApiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this).addApi(Plus.API, null)
				.addScope(Plus.SCOPE_PLUS_LOGIN).build();
		 */
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.FragmentActivity#onStart()
	 */
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		mPlusClient.connect();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v7.app.ActionBarActivity#onStop()
	 */
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		mPlusClient.disconnect();
	}

}
