package com.davide.parise.radioexample.podcast;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import org.eclipse.jdt.annotation.NonNull;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.davide.parise.radioexample.R;
import com.davide.parise.radioexample.lib.JSonParser;

public class PodcastFragment extends Fragment{
	private static final String FIRST_URL = "https://enacast.com/api/v1/llistaprogrames?radio=1";
	private static final String SECOND_URL = "https://enacast.com/api/v1/llistapodcasts?radio=radiodesvern&programa=laudicio&limit=365";
	
	ListView listPodcast;
	ProgressBar progress;
	ArrayAdapterPodcast listViewAdapter;
	
	private OnItemClickListener listener_listView = new OnItemClickListener(){

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
			// TODO Auto-generated method stub
			
			Worker worker = new Worker();
			worker.setExternListener(workerListener);
			worker.execute(SECOND_URL);
		}
		
	};

	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onAttach(android.app.Activity)
	 */
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		Log.i(PodcastFragment.class.getSimpleName(), "onAttach");
		listViewAdapter = new ArrayAdapterPodcast(activity);
	}

	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onActivityCreated(android.os.Bundle)
	 */
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		Log.i(PodcastFragment.class.getSimpleName(), "onActivityCreated: start worker");
		Worker worker = new Worker();
		worker.setExternListener(workerListener);
		worker.execute(FIRST_URL);
		
	}

	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		ViewGroup rootView = (ViewGroup)inflater.inflate(R.layout.podcast_fragment, container, false);
		return rootView;
	}

	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onViewCreated(android.view.View, android.os.Bundle)
	 */
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		Log.i(PodcastFragment.class.getSimpleName(), "onViewCreated");
		listPodcast = (ListView) view.findViewById(R.id.listView_podcast_fragment);
		listPodcast.setOnItemClickListener(listener_listView);
		listPodcast.setAdapter(listViewAdapter);
		progress = (ProgressBar) view.findViewById(R.id.progressBar_podcast_fragment);
	}

	/***************************************************************/
	/************************* WORKER ******************************/
	/***************************************************************/
	
	
	private Worker.WorkerListener workerListener = new Worker.WorkerListener(){

		
		@Override
		public void onPreExcecute() {
			// TODO Auto-generated method stub
			Log.d(PodcastFragment.class.getSimpleName(), "onPreExcecute");
			listPodcast.setVisibility(View.GONE);
			progress.setVisibility(View.VISIBLE);
		}

		@Override
		public void onPostExecute(List<String> list) {
			// TODO Auto-generated method stub
			Log.d(PodcastFragment.class.getSimpleName(), "onPostExcecute");
			progress.setVisibility(View.GONE);
			listPodcast.setVisibility(View.VISIBLE);

			listViewAdapter.setElement(list);
			listViewAdapter.notifyDataSetChanged();
		}

		@Override
		public void onExceptionFound(@NonNull IOException exception) {
			// TODO Auto-generated method stub
			
			// handle exception here ... 
			Log.e(PodcastFragment.class.getSimpleName(), exception.getMessage());
			Toast.makeText(getActivity(),exception.getMessage(), Toast.LENGTH_LONG).show();
			
			progress.setVisibility(View.GONE);
			listPodcast.setVisibility(View.VISIBLE);

			listViewAdapter.setElement(null);

		}
		
	};
	
	/**
	 * 
	 * @author davide
	 * Static class that retrieve info from URL
	 *
	 */
	private static class Worker extends AsyncTask<String,String,List<String>>{
		private WorkerListener externListener;
		private IOException exception;
		
		public Worker() {} // empy constructo

		public interface WorkerListener{
			public void onPreExcecute();
			public void onPostExecute(List<String> list);
			public void onExceptionFound(@NonNull IOException exception);
		}
		
		@Override
		protected List<String>  doInBackground(String... params) {
			// TODO Auto-generated method stub
			URL url=null;
			try {
				url = new URL(params[0]);
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				this.exception = e;
				return null;
			}
			
			try {
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				InputStream in = conn.getInputStream();
				JSonParser reader = new JSonParser();
				
				return reader.readElement(in);
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				this.exception = e;
				return null;
			}
		}

		/* (non-Javadoc)
		 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
		 */
		@Override
		protected void onPostExecute(List<String> result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if(this.externListener!=null){
				if(this.exception!=null)
					this.externListener.onExceptionFound(exception);
				else
					this.externListener.onPostExecute(result);
			}
		}

		/* (non-Javadoc)
		 * @see android.os.AsyncTask#onPreExecute()
		 */
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			if(this.externListener!=null)
				this.externListener.onPreExcecute();
		}

		/*************************************************************/
		/************************* SET & GET *************************/
		/*************************************************************/

		/**
		 * @return the externListener
		 */
		@SuppressWarnings("unused")
		public WorkerListener getExternListener() {
			return externListener;
		}

		/**
		 * @param externListener the externListener to set
		 */
		public void setExternListener(WorkerListener externListener) {
			this.externListener = externListener;
		}
		
		
	}

}
