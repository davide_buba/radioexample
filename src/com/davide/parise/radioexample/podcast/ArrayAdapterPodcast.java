package com.davide.parise.radioexample.podcast;

import java.util.ArrayList;
import java.util.List;

import com.davide.parise.radioexample.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ArrayAdapterPodcast extends BaseAdapter {

	private List<String> element;
	private Context context;
	
	
	public ArrayAdapterPodcast(Context context, List<String> element) {
		super();
		this.element = element;
		if(this.element==null)
			this.element = new ArrayList<String>();
		this.context = context;
	}

	public ArrayAdapterPodcast(Context context) {
		this.context = context;
		this.element = new ArrayList<String>();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if(this.element==null)
			return 0;
		return this.element.size();
	}

	@Override
	public String getItem(int position) {
		// TODO Auto-generated method stub
		return this.element.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		LayoutInflater inflater = LayoutInflater.from(context);
		if (convertView == null) {
			convertView = inflater.inflate(com.davide.parise.radioexample.R.layout.podcast_list_row, parent, false);
		}
		TextView text = (TextView) convertView.findViewById(R.id.textView_podcast_list_title);
		text.setText(element.get(position));
		
		return convertView;
	}

	/**
	 * @return the element
	 */
	public List<String> getElement() {
		return element;
	}

	/**
	 * @param element the element to set
	 */
	public void setElement(List<String> element) {
		this.element = element;
		if(this.element==null)
			this.element = new ArrayList<String>(); // never null
		
	}

	
}
