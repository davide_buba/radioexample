package com.davide.parise.radioexample;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.davide.parise.radioexample.onair.OnairFragment;
import com.davide.parise.radioexample.podcast.PodcastFragment;
import com.davide.parise.radioexample.social.SocialFragment;

public class MainPageAdapter extends FragmentPagerAdapter {

	public static final int NUM_PAGES = 3; // number of total pages
	
	// constant that identify the page class
	public static final int PAGE_ONAIR = 0;
	public static final int PAGE_PODCAST = 1;
	public static final int PAGE_SOCIAL = 2;

	private final String[] title = {
			"OnAir",
			"Podcast",
			"social"
	};
	
	public MainPageAdapter(FragmentManager fm) {
		super(fm);
		// TODO Auto-generated constructor stub
	}


	@Override
	public Fragment getItem(int arg0) {
		// TODO Auto-generated method stub
		Fragment result = null;
		
		switch(arg0){
		case PAGE_ONAIR:
			result = new OnairFragment();
			break;
		case PAGE_PODCAST:
			result = new PodcastFragment();
			break;
		case PAGE_SOCIAL:
			result = new SocialFragment();
			break;
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see android.support.v4.view.PagerAdapter#getPageTitle(int)
	 */
	@Override
	public CharSequence getPageTitle(int position) {
		// TODO Auto-generated method stub
		return title[position];
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return NUM_PAGES;
	}

	/* (non-Javadoc)
	 * @see android.support.v4.view.PagerAdapter#getItemPosition(java.lang.Object)
	 */
	@Override
	public int getItemPosition(Object object) {
		// TODO Auto-generated method stub
		if(object instanceof OnairFragment)
			return PAGE_ONAIR;
		else if(object instanceof PodcastFragment)
			return PAGE_PODCAST;
		else if(object instanceof SocialFragment)
			return PAGE_SOCIAL;
		return super.getItemPosition(object);
	}

}
